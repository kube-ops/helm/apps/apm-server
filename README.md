# apm-server

[![Version: 0.1.2](https://img.shields.io/badge/Version-0.1.2-informational?style=flat-square) ](#)
[![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ](#)
[![AppVersion: 7.17.6](https://img.shields.io/badge/AppVersion-7.17.6-informational?style=flat-square) ](#)
[![Artifact Hub: kube-ops](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/kube-ops&style=flat-square)](https://artifacthub.io/packages/helm/kube-ops/apm-server)

The APM Server receives data from Elastic APM agents and transforms it into Elasticsearch documents

## Installing the Chart

To install the chart with the release name `my-release`:

```console
$ helm repo add kube-ops https://charts.kube-ops.io
$ helm repo update
$ helm upgrade my-release kube-ops/apm-server --install --namespace my-namespace --create-namespace --wait
```

## Uninstalling the Chart

To uninstall the chart:

```console
$ helm uninstall my-release --namespace my-namespace
```

## Updating CRDs

```console
$ kubectl apply -k https://gitlab.com/kube-ops/helm/apps/traefik
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity |
| agentConfigCacheExpiration | string | `"30s"` | When using APM agent configuration, information fetched from Kibana will be cached in memory for some time. Specify cache key expiration via this setting. Default is 30 seconds. |
| autoscaling.enabled | bool | `false` | Specifies the horizontal pod autoscaling is enabled. Only used if deploymentType == Deployment. |
| autoscaling.maxReplicas | int | `100` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| capturePersonalData | bool | `true` | If true, APM Server captures the IP of the instrumented service or the IP and User Agent of the real user (RUM requests). |
| configFilename | string | `"/etc/apm-server/config.yml"` |  |
| configOverride | string | `""` | - Config file and paths |
| console | object | `{"json":{"escapeHtml":false,"pretty":false}}` | - Output: Console |
| console.json | object | `{"escapeHtml":false,"pretty":false}` | Configure JSON encoding. |
| console.json.escapeHtml | bool | `false` | Configure escaping HTML symbols in strings. |
| console.json.pretty | bool | `false` | Pretty-print JSON event. |
| containerPort | int | `8200` |  |
| dataDir.emptyDir | object | `{}` |  |
| dataDir.mountPath | string | `"/usr/share/apm-server/data"` |  |
| defaultServiceEnvironment | string | `""` | If specified, APM Server will record this value in events which have no service environment defined, and add it to agent configuration queries to Kibana when none is specified in the request from the agent. |
| deploymentType | string | `"Deployment"` | Type of the application controller. Possible values: Deployment, DaemonSet |
| dnsConfig | object | `{}` |  |
| dnsPolicy | string | `""` | Pod-specific DNS policy. If hostNetwork is set to true, use ClusterFirstWithHostNet dnsPolicy value. |
| elasticsearch | object | `{"backoffInit":"1s","backoffMax":"60s","bulkMaxSize":50,"compressionLevel":0,"headers":{},"hosts":["elasticsearch:9200"],"index":"apm-%{[observer.version]}-%{+yyyy.MM.dd}","indices":[],"maxRetries":3,"path":"","pipeline":"","proxyUrl":"","timeout":90,"worker":1}` | - Output: ElasticSearch |
| elasticsearch.backoffInit | string | `"1s"` | The number of seconds to wait before trying to reconnect to Elasticsearch after a network error. After waiting backoff.init seconds, apm-server tries to reconnect. If the attempt fails, the backoff timer is increased exponentially up to backoff.max. After a successful connection, the backoff timer is reset. The default is 1s. |
| elasticsearch.backoffMax | string | `"60s"` | The maximum number of seconds to wait before attempting to connect to Elasticsearch after a network error. The default is 60s. |
| elasticsearch.bulkMaxSize | int | `50` | The maximum number of events to bulk in a single Elasticsearch bulk API index request. The default is 50. |
| elasticsearch.compressionLevel | int | `0` | Set gzip compression level. |
| elasticsearch.headers | object | `{}` | Custom HTTP headers to add to each request. |
| elasticsearch.hosts | list | `["elasticsearch:9200"]` | Array of hosts to connect to. Scheme and port can be left out and will be set to the default (`http` and `9200`). In case you specify and additional path, the scheme is required: `http://localhost:9200/path`. IPv6 addresses should always be defined as: `https://[2001:db8::1]:9200`. |
| elasticsearch.index | string | `"apm-%{[observer.version]}-%{+yyyy.MM.dd}"` | By using the configuration below, APM documents are stored to separate indices, depending on their `processor.event`: - error - transaction - span - sourcemap The indices are all prefixed with `apm-%{[observer.version]}`. To allow managing indices based on their age, all indices (except for sourcemaps) end with the information of the day they got indexed. e.g. "apm-7.3.0-transaction-2019.07.20" Be aware that you can only specify one Elasticsearch template. If you modify the index patterns you must also update these configurations accordingly, as they need to be aligned: * `setupTemplate.name` * `setupTemplate.pattern` |
| elasticsearch.maxRetries | int | `3` | The number of times a particular Elasticsearch index operation is attempted. If the indexing operation doesn't succeed after this many retries, the events are dropped. The default is 3. |
| elasticsearch.path | string | `""` | Optional HTTP Path. |
| elasticsearch.pipeline | string | `""` | A pipeline is a definition of processors applied to documents when ingesting them to Elasticsearch. APM Server comes with a default pipeline definition, located at `ingest/pipeline/definition.json`, which is loaded to Elasticsearch by default (see `apm-server.register.ingest.pipeline`). APM pipeline is enabled by default. To disable it, set `pipeline: _none`. |
| elasticsearch.proxyUrl | string | `""` | Proxy server url. |
| elasticsearch.timeout | int | `90` | Configure http request timeout before failing an request to Elasticsearch. |
| elasticsearch.worker | int | `1` | Number of workers per Elasticsearch host. |
| existingConfigMapName | string | `""` |  |
| extraEnvVars | list | `[]` | Additional environment variables |
| extraVolumeMounts | list | `[]` | Additional mounts for application |
| extraVolumes | list | `[]` | Additional volumes for application |
| fullnameOverride | string | `""` | Overrides the full name |
| global | object | `{"imagePullPolicy":"IfNotPresent","imagePullSecrets":[]}` | - Global |
| global.imagePullPolicy | string | `"IfNotPresent"` | Image download policy ref: https://kubernetes.io/docs/concepts/containers/images/#updating-images |
| global.imagePullSecrets | list | `[]` | List of the Docker registry credentials ref: https://kubernetes.io/docs/concepts/containers/images/#updating-images |
| hostAliases | list | `[]` | Adding entries to Pod /etc/hosts with HostAliases |
| hostNetwork | bool | `false` | Specifies whether a container ports should be exposed to host. |
| httpPort | int | `0` |  |
| idleTimeout | string | `"45s"` | Maximum amount of time to wait for the next incoming request before underlying connection is closed. |
| image | object | `{"repository":"docker.io/elastic/apm-server","tag":""}` | - Pod and container configuration |
| image.repository | string | `"docker.io/elastic/apm-server"` | Overrides the image repository |
| image.tag | string | `""` | Overrides the image tag whose default is the chart appVersion. |
| indexLifecycleManagement.enabled | string | `"auto"` | Supported values are `auto`, `true` and `false`. `true`: Make use of Elasticsearch's Index Lifecycle Management (ILM) for APM indices. If no Elasticsearch output is configured or the configured instance does not support ILM, APM Server cannot apply ILM and must create unmanaged indices instead. `false`: APM Server does not make use of ILM in Elasticsearch. `auto`: If an Elasticsearch output is configured with default index and indices settings, and the configured Elasticsearch instance supports ILM, `auto` will resolve to `true`. Otherwise `auto` will resolve to `false`. Default value is `auto`. |
| indexLifecycleManagement.setup.enabled | bool | `true` | Only disable setup if you want to set up everything related to ILM on your own. When setup is enabled, the APM Server creates: - aliases and ILM policies if `apm-server.ilm.enabled` resolves to `true`. - An ILM specific template per event type. This is required to map ILM aliases and policies to indices. In case ILM is disabled, the templates will be created without any ILM settings. Be aware that if you turn off setup, you need to manually manage event type specific templates on your own. If you simply want to disable ILM, use the above setting, `apm-server.ilm.enabled`, instead. |
| indexLifecycleManagement.setup.mapping | list | `[]` | Customized mappings will be merged with the default setup, so you only need to configure mappings for the event types, policies, and index suffixes that you want to customize. Indices are named in this way: `apm-%{[observer.version]}-%{[event.type]}-{index_suffix}`, e.g., apm-7.9.0-span-custom*. The `index_suffix` is optional. NOTE: When configuring an `index_suffix`, ensure that no previously set up templates conflict with the       newly configured ones. If an index matches multiple templates with the same order, the settings of       the templates will override each other. Any conflicts need to be cleaned up manually. NOTE: When customizing `setup.template.name` and `setup.template.pattern`, ensure they still match the indices. |
| indexLifecycleManagement.setup.overwrite | bool | `false` | Configure whether or not existing policies and ILM related templates should be updated. This needs to be set to true when customizing your policies. |
| indexLifecycleManagement.setup.policies | list | `[]` |  |
| indexLifecycleManagement.setup.requirePolicy | bool | `true` | Set to `false` when policies are set up outside of APM Server but referenced here. |
| initContainers | list | `[]` | Specifies init containers |
| jaeger | object | `{"grpc":{"authTag":"","enabled":false,"host":"0.0.0.0:14250"},"http":{"enabled":false,"host":"0.0.0.0:14268"}}` | When enabling Jaeger integration, APM Server acts as Jaeger collector. It supports jaeger.thrift over HTTP and gRPC. This is an experimental feature, use with care. WARNING: This configuration is deprecated, and will be removed in the 8.0 release. Jaeger gRPC is now served on the same port as Elastic APM agents, defined by the "apm-server.host" configuration; it is implicitly enabled, and an agent tag called "elastic-apm-auth" is required when auth is enabled. |
| jaeger.grpc.authTag | string | `""` | The tag value should have the same format as an HTTP Authorization header, i.e. "Bearer <secret_token>" or |
| jaeger.grpc.enabled | bool | `false` | Set to true to enable the Jaeger gRPC collector service. |
| jaeger.grpc.host | string | `"0.0.0.0:14250"` | Defines the gRPC host and port the server is listening on. Defaults to the standard Jaeger gRPC collector port 14250. |
| jaeger.http.enabled | bool | `false` | Set to true to enable the Jaeger HTTP collector endpoint. |
| jaeger.http.host | string | `"0.0.0.0:14268"` | Defines the HTTP host and port the server is listening on. Defaults to the standard Jaeger HTTP collector port 14268. |
| kibana.enabled | bool | `false` | For APM Agent configuration in Kibana, enabled must be true. |
| kibana.host | string | `"localhost:5601"` | Scheme and port can be left out and will be set to the default (`http` and `5601`). In case you specify an additional path, the scheme is required: `http://localhost:5601/path`. IPv6 addresses should always be defined as: `https://[2001:db8::1]:5601`. |
| livenessProbe | object | `{}` |  |
| logJson | bool | `true` |  |
| logLevel | string | `"info"` | Sets the minimum log level. The default log level is info. Available log levels are: error, warning, info, or debug. |
| logsDir.emptyDir | object | `{}` |  |
| logsDir.mountPath | string | `"/usr/share/apm-server/logs"` |  |
| logstash | object | `{"backoffInit":"1s","backoffMax":"60s","compressionLevel":3,"escapeHtml":true,"hosts":["localhost:5044"],"index":"","loadbalance":false,"pipelining":2,"proxyUrl":"","proxyUseLocalResolver":false,"slowStart":false,"ttl":"30s","worker":1}` | - Output: Logstash |
| logstash.backoffInit | string | `"1s"` | The number of seconds to wait before trying to reconnect to Logstash after a network error. After waiting backoffInit seconds, apm-server tries to reconnect. If the attempt fails, the backoff timer is increased exponentially up to backoffMax. After a successful connection, the backoff timer is reset. |
| logstash.backoffMax | string | `"60s"` | The maximum number of seconds to wait before attempting to connect to Logstash after a network error. |
| logstash.compressionLevel | int | `3` | Set gzip compression level. |
| logstash.escapeHtml | bool | `true` | Configure escaping html symbols in strings. |
| logstash.index | string | `""` | Optional index name. |
| logstash.loadbalance | bool | `false` | Optional load balance the events between the Logstash hosts. Default is false. |
| logstash.pipelining | int | `2` | Number of batches to be sent asynchronously to Logstash while processing new batches. |
| logstash.proxyUrl | string | `""` | SOCKS5 proxy server URL |
| logstash.proxyUseLocalResolver | bool | `false` | Resolve names locally when using a proxy server. |
| logstash.slowStart | bool | `false` | If enabled only a subset of events in a batch of events is transferred per group. The number of events to be sent increases up to `bulkMaxSize` if no error is encountered. |
| logstash.ttl | string | `"30s"` | Optional maximum time to live for a connection to Logstash, after which the connection will be re-established.  A value of `0s` (the default) will disable this feature. Not yet supported for async connections (i.e. with the "pipelining" option set). |
| logstash.worker | int | `1` | Number of workers per Logstash host. |
| maxConnections | int | `0` | Maximum number of new connections to accept simultaneously (0 means unlimited). |
| maxEventSize | int | `307200` | Maximum permitted size in bytes of an event accepted by the server to be processed. |
| maxHeaderSize | int | `1048576` | Maximum permitted size in bytes of a request's header accepted by the server to be processed. |
| maxProcs | int | `0` | Sets the maximum number of CPUs that can be executing simultaneously. The default is the number of logical CPUs available in the system. |
| nameOverride | string | `""` | Overrides the chart name |
| nodeSelector | object | `{}` |  |
| outputType | string | `"console"` | - Output |
| pdb.enabled | bool | `false` | Specifies whether a pod disruption budget should be created |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{"fsGroup":1000}` | Pod security settings |
| podSecurityPolicy.allowedCapabilities | list | `[]` | Linux capabilities, allowed by PodSecurityPolicy. If you run apm-server listened port <1024, you must add the NET_BIND_SERVICE capability. ref: https://kubernetes.io/docs/concepts/policy/pod-security-policy/#capabilities |
| podSecurityPolicy.annotations | object | `{}` |  |
| podSecurityPolicy.create | bool | `true` | Specifies whether a pod security policy should be created |
| podSecurityPolicy.enabled | bool | `false` | Specifies whether a pod security policy should be enabled |
| podSecurityPolicy.name | string | `""` | The name of the pod security policy to use. If not set and create is true, a name is generated using the fullname template |
| preStopHook | object | `{}` |  |
| priority | int | `0` |  |
| priorityClassName | string | `""` | Overrides default priority class ref: https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/ |
| queue | object | `{"mem":{"events":4096,"flushMinEvents":2048,"flushTimeout":"1s"}}` | Data is buffered in a memory queue before it is published to the configured output. The memory queue will present all available events (up to the outputs bulk_max_size) to the output, the moment the output is ready to serve another batch of events. |
| queue.mem | object | `{"events":4096,"flushMinEvents":2048,"flushTimeout":"1s"}` | Queue type by name (default 'mem'). |
| queue.mem.events | int | `4096` | Max number of events the queue can buffer. |
| queue.mem.flushMinEvents | int | `2048` | Hints the minimum number of events stored in the queue, before providing a batch of events to the outputs. The default value is set to 2048. A value of 0 ensures events are immediately available to be sent to the outputs. |
| queue.mem.flushTimeout | string | `"1s"` | Maximum duration after which events are available to the outputs, if the number of events stored in the queue is < `flush.min_events`. |
| rbac.create | bool | `false` | Specifies whether a cluster role should be created |
| rbac.name | string | `""` | The name of the cluster role to use. If not set and create is true, a name is generated using the fullname template |
| readTimeout | string | `"30s"` | Maximum permitted duration for reading an entire request. |
| readinessProbe | object | `{}` |  |
| registerIngestPipeline | object | `{"enabled":true,"overwrite":false}` | You can manually register a pipeline, or use this configuration option to ensure the pipeline is loaded and registered at the configured Elasticsearch instances. Find the default pipeline configuration at `ingest/pipeline/definition.json`. Automatic pipeline registration requires the `output.elasticsearch` to be enabled and configured. |
| registerIngestPipeline.enabled | bool | `true` | Registers APM pipeline definition in Elasticsearch on APM Server startup. Defaults to true. |
| registerIngestPipeline.overwrite | bool | `false` | Overwrites existing APM pipeline definition in Elasticsearch. Defaults to false. |
| replicaCount | int | `1` | Replicas count. Only used if deploymentType == Deployment and autoscaling disabled. |
| resources | object | `{}` | Application container resources configuration We usually recommend not to specify default resources and to leave this as a conscious choice for the user. This also increases chances charts run on environments with little resources, such as Minikube. If you do want to specify resources, uncomment the following lines, adjust them as necessary, and remove the curly braces after 'resources:'. |
| responseHeaders | object | `{}` | Custom HTTP headers to add to all HTTP responses, e.g. for security policy compliance. |
| rum | object | `{"allowHeaders":[],"allowOrigins":["*"],"enabled":false,"eventRateLimit":300,"eventRateLimitCacheSize":1000,"excludeFromGrouping":[],"libraryPattern":[],"responseHeaders":{},"sourceMapping":{"cacheExpiration":"5m","enabled":true,"indexPattern":"apm-*-sourcemap*","timeout":"5s"}}` | - RUM Real User Monitoring |
| rum.allowHeaders | list | `[]` | A list of Access-Control-Allow-Headers to allow RUM requests, in addition to "Content-Type", "Content-Encoding", and "Accept" |
| rum.allowOrigins | list | `["*"]` | A list of permitted origins for real user monitoring. User-agents will send an origin header that will be validated against this list. An origin is made of a protocol scheme, host and port, without the url path. Allowed origins in this setting can have * to match anything (eg.: http://*.example.com) If an item in the list is a single '*', everything will be allowed. |
| rum.enabled | bool | `false` | Enable Real User Monitoring (RUM) Support. By default RUM is disabled. RUM does not support token based authorization. Enabled RUM endpoints will not require any authorization token configured for other endpoints. |
| rum.eventRateLimit | int | `300` | Defines the maximum amount of events allowed to be sent to the APM Server RUM endpoint per IP per second. |
| rum.eventRateLimitCacheSize | int | `1000` | An LRU cache is used to keep a rate limit per IP for the most recently seen IPs. This setting defines the number of unique IPs that can be tracked in the cache. Sites with many concurrent clients should consider increasing this limit. Defaults to 1000. |
| rum.excludeFromGrouping | list | `[]` | Regexp to be matched against a stacktrace frame's `file_name`. If the regexp matches, the stacktrace frame is not used for calculating error groups. The default pattern excludes stacktrace frames that have a filename starting with '/webpack' |
| rum.libraryPattern | list | `[]` | Regexp to be matched against a stacktrace frame's `file_name` and `abs_path` attributes. If the regexp matches, the stacktrace frame is considered to be a library frame. |
| rum.responseHeaders | object | `{}` | Custom HTTP headers to add to RUM responses, e.g. for security policy compliance. |
| rum.sourceMapping | object | `{"cacheExpiration":"5m","enabled":true,"indexPattern":"apm-*-sourcemap*","timeout":"5s"}` | If a source map has previously been uploaded, source mapping is automatically applied. to all error and transaction documents sent to the RUM endpoint. |
| rum.sourceMapping.cacheExpiration | string | `"5m"` | Determines how long a source map should be cached before fetching it again from Elasticsearch. Note that values configured without a time unit will be interpreted as seconds. |
| rum.sourceMapping.enabled | bool | `true` | Sourcemapping is enabled by default. |
| rum.sourceMapping.indexPattern | string | `"apm-*-sourcemap*"` | Source maps are stored in a separate index. If the default index pattern for source maps at 'elasticsearch.indices' is changed, a matching index pattern needs to be specified here. |
| rum.sourceMapping.timeout | string | `"5s"` | Timeout for fetching source maps. |
| runtimeClassName | string | `""` | Overrides default runtime class |
| schedulerName | string | `""` | Overrides default scheduler |
| secretToken | string | `""` | Define a shared secret token for authorizing agents using the "Bearer" authorization method. |
| securityContext | object | `{"capabilities":{"drop":["ALL"]},"readOnlyRootFilesystem":true,"runAsGroup":1000,"runAsNonRoot":true,"runAsUser":1000}` | Contaier security settings |
| service.annotations | object | `{}` | Annotations for Service resource |
| service.clusterIP | string | `""` | Exposes the Service on a cluster IP ref: https://kubernetes.io/docs/concepts/services-networking/service/#choosing-your-own-ip-address |
| service.externalIPs | list | `[]` | If there are external IPs that route to one or more cluster nodes, Kubernetes Services can be exposed on those externalIPs ref: https://kubernetes.io/docs/concepts/services-networking/service/#external-ips |
| service.externalName | string | `""` | Services of type ExternalName map a Service to a DNS name ref: https://kubernetes.io/docs/concepts/services-networking/service/#externalname |
| service.externalTrafficPolicy | string | `"Cluster"` | If you set service.spec.externalTrafficPolicy to the value Local, kube-proxy only proxies proxy requests to local endpoints, and does not forward traffic to other nodes. This approach preserves the original source IP address. If there are no local endpoints, packets sent to the node are dropped, so you can rely on the correct source-ip in any packet processing rules you might apply a packet that make it through to the endpoint. ref: https://kubernetes.io/docs/tutorials/services/source-ip/#source-ip-for-services-with-type-nodeport |
| service.ipFamily | string | `""` | Address family for the Service's cluster IP (IPv4 or IPv6) ref: https://kubernetes.io/docs/concepts/services-networking/dual-stack/ |
| service.loadBalancerSourceRanges | list | `[]` | If is not set, Kubernetes allows traffic from 0.0.0.0/0 to the Node Security Group(s). ref: https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer |
| service.port | int | `8200` |  |
| service.topology | bool | `false` | enables a service to route traffic based upon the Node topology of the cluster ref: https://kubernetes.io/docs/concepts/services-networking/service-topology/#using-service-topology Kubernetes >= kubeVersion 1.18 |
| service.topologyKeys | list | `[]` |  |
| service.type | string | `"ClusterIP"` | Kubernetes ServiceTypes allow you to specify what kind of Service you want ref: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types |
| serviceAccount | object | `{"annotations":{},"create":false,"name":""}` | - Security |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `false` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| setupTemplate | object | `{"enabled":true,"fields":"","name":"apm-%{[observer.version]}","overwrite":false,"pattern":"apm-%{[observer.version]}-*","settings":{"index":{"codec":"best_compression","mappingTotalFieldsLimit":2000,"numberOfRoutingShards":30,"numberOfShards":1}}}` | A template is used to set the mapping in Elasticsearch. By default template loading is enabled and the template is loaded. These settings can be adjusted to load your own template or overwrite existing ones. |
| setupTemplate.enabled | bool | `true` | Set to false to disable template loading. |
| setupTemplate.fields | string | `""` | Path to fields.yml file to generate the template. |
| setupTemplate.name | string | `"apm-%{[observer.version]}"` | Template name. By default the template name is "apm-%{[observer.version]}" The template name and pattern has to be set in case the elasticsearch index pattern is modified. |
| setupTemplate.overwrite | bool | `false` | Overwrite existing template. |
| setupTemplate.pattern | string | `"apm-%{[observer.version]}-*"` | Template pattern. By default the template pattern is "apm-%{[observer.version]}-*" to apply to the default index settings. The first part is the version of apm-server and then -* is used to match all daily indices. The template name and pattern has to be set in case the elasticsearch index pattern is modified. |
| setupTemplate.settings | object | `{"index":{"codec":"best_compression","mappingTotalFieldsLimit":2000,"numberOfRoutingShards":30,"numberOfShards":1}}` | Elasticsearch template settings. |
| setupTemplate.settings.index | object | `{"codec":"best_compression","mappingTotalFieldsLimit":2000,"numberOfRoutingShards":30,"numberOfShards":1}` | A dictionary of settings to place into the settings.index dictionary of the Elasticsearch template. For more details, please check https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html |
| shutdownTimeout | string | `"5s"` | Maximum duration before releasing resources when shutting down the server. |
| startupProbe | object | `{}` |  |
| terminationGracePeriodSeconds | int | `30` | Grace period before the Pod is allowed to be forcefully killed ref: https://kubernetes.io/docs/concepts/containers/container-lifecycle-hooks/ |
| tolerations | list | `[]` |  |
| updateStrategy | object | `{}` |  |
| writeTimeout | string | `"30s"` | Maximum permitted duration for writing a response. |

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.kube-ops.io | generate | ~0.2.3 |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.8.1](https://github.com/norwoodj/helm-docs/releases/v1.8.1)
