{{- define "apm-server.config" -}}
{{-   if .Values.configOverride -}}
{{      .Values.configOverride }}
{{-   else -}}
apm-server:
  host: {{ printf "0.0.0.0:%d" (int .Values.containerPort) }}
  {{-   if .Values.secretToken }}
  auth:
    secret_token: ${ELASTIC_APM_SECRET_TOKEN}
  {{-   end }}
  max_header_size: {{ int .Values.maxHeaderSize }}
  idle_timeout: {{ .Values.idleTimeout }}
  read_timeout: {{ .Values.readTimeout }}
  write_timeout: {{ .Values.writeTimeout }}
  shutdown_timeout: {{ .Values.shutdownTimeout }}
  max_event_size: {{ int .Values.maxEventSize }}
  max_connections: {{ .Values.maxConnections }}
  {{-   if .Values.responseHeaders }}
  {{-     with .Values.responseHeaders }}
  response_headers:
    {{-     toYaml . nindent 4 }}
  {{-     end }}
  {{-   end }}
  capture_personal_data: {{ .Values.capturePersonalData }}
  {{-   if .Values.defaultServiceEnvironment }}
  default_service_environment: {{ .Values.defaultServiceEnvironment }}
  {{-   end }}
  {{-   if and .Values.registerIngestPipeline.enabled (eq .Values.outputType "elasticsearch") }}
  register.ingest.pipeline:
    enabled: true
    overwrite: {{ .Values.registerIngestPipeline.overwrite }}
  {{-   end }}
  {{-   if .Values.rum.enabled }}
  #---------------------------- APM Server - RUM Real User Monitoring ----------------------------
  rum:
    enabled: true
    event_rate:
      limit: {{ .Values.rum.eventRateLimit }}
      lru_size: {{ .Values.rum.eventRateLimitCacheSize }}
    {{-   if .Values.rum.allowOrigins }}
    {{-     with .Values.rum.allowOrigins }}
    allow_origins:
      {{-     toYaml . | nindent 6 }}
    {{-     end }}
    {{-   else }}
    allow_origins: []
    {{-   end }}
    {{-   if .Values.rum.allowHeaders }}
    {{-     with .Values.rum.allowHeaders }}
    allow_headers:
      {{-     toYaml . | nindent 6 }}
    {{-     end }}
    {{-   else }}
    allow_headers: []
    {{-   end }}
    {{-   if .Values.rum.responseHeaders }}
    {{-     with .Values.rum.responseHeaders }}
    response_headers:
      {{-     toYaml . | nindent 6 }}
    {{-     end }}
    {{-   end }}
    {{-   if .Values.rum.libraryPattern }}
    library_pattern: {{ join .Values.rum.libraryPattern "|" | quote }}
    {{-   end }}
    {{-   if .Values.rum.excludeFromGrouping }}
    exclude_from_grouping: {{ join .Values.rum.excludeFromGrouping "|" | quote }}
    {{-   end }}
    source_mapping:
    {{-   if .Values.rum.sourceMapping }}
      enabled: true
      timeout: {{ .Values.rum.sourceMapping.timeout }}
      {{-   if eq .Values.outputType "elasticsearch" }}
      elasticsearch:
        {{-   with .Values.elasticsearch.hosts }}
        hosts:
          {{- toYaml . | nindent 10 }}
        {{-   end }}
      {{-   end }}
      cache:
        expiration: {{- .Values.rum.sourceMapping.cacheExpiration }}
      index_pattern: {{- .Values.rum.sourceMapping.indexPattern | quote }}
  {{-     else }}
      enabled: false
  {{-     end }}
  {{-   end }}
  #---------------------------- APM Server - Agent Configuration ----------------------------
  agent.config.cache.expiration: {{ .Values.agentConfigCacheExpiration }}
  kibana:
    enabled: {{ .Values.kibana.enabled }}
    {{- if .Values.kibana.enabled }}
    host: {{ .Values.kibana.host | quote }}
    protocol: "http"
    ssl.enabled: false
    {{- end }}
  #---------------------------- APM Server - ILM Index Lifecycle Management ----------------------------
  ilm:
    enabled: {{ .Values.indexLifecycleManagement.enabled | quote }}
    setup:
      enabled: {{ .Values.indexLifecycleManagement.setup.enabled }}
      overwrite: {{ .Values.indexLifecycleManagement.setup.overwrite }}
      require_policy: {{ .Values.indexLifecycleManagement.setup.requirePolicy }}
      {{- if .Values.indexLifecycleManagement.setup.mapping }}
      mapping:
        {{- range $i, $val := .Values.indexLifecycleManagement.setup.mapping }}
        - event_type: {{ $val.eventType | quote }}
          policy_name: {{ $val.policyName | quote }}
          index_suffix: {{ $val.indexSuffix | quote }}
        {{- end }}
      {{- end }}
      {{- if .Values.indexLifecycleManagement.setup.policies }}
      policies:
        {{- range $i, $val := .Values.indexLifecycleManagement.setup.mapping }}
        - event_type:
          policy_name: {{ $val.policyName | quote }}
          index_suffix: {{ $val.indexSuffix | quote }}
        - name: {{ $val.name | quote }}
          policy:
            {{- if $val.phases }}
            phases:
              {{- if $val.phases.hot }}
              hot:
                {{- if $val.phases.hot.actions }}
                actions:
                  {{- if $val.phases.hot.actions.rollover }}
                  rollover:
                    max_size: {{ $val.phases.hot.actions.rollover.maxSize | quote }}
                    max_age: {{ $val.phases.hot.actions.rollover.maxAge | quote }}
                  {{- end }}
                  {{- if $val.phases.hot.actions.setPriority }}
                  set_priority:
                    priority: {{ $val.phases.hot.actions.setPriority.priority }}
                  {{- end }}
                {{- end }}
              {{- end }}
            {{- end }}
        {{- end }}
      {{- end }}
  {{-   if eq .Values.outputType "jaeger" }}
  #---------------------------- APM Server - Experimental Jaeger integration ----------------------------
  jaeger:
    grpc:
      enabled: {{ .Values.jaeger.grpc.enabled }}
      host: {{ .Values.jaeger.grpc.host | quote }}
      {{- if .Values.jaeger.grpc.authTag }}
      auth_tag: {{ .Values.jaeger.grpc.authTag | quote }}
      {{- end }}
    http:
      enabled: {{ .Values.jaeger.http.enabled }}
      host: {{ .Values.jaeger.http.host | quote }}
  {{-   end }}
#================================= General =================================
queue:
  mem:
    events: {{ int .Values.queue.mem.events }}
    flush.min_events: {{ int .Values.queue.mem.flushMinEvents }}
    flush.timeout: {{ .Values.queue.mem.flushTimeout }}
{{-     if gt (int .Values.maxProcs) 0 }}
max_procs: {{ int .Values.maxProcs }}
{{-     end }}
{{-     if and .Values.setupTemplate.enabled (eq .Values.outputType "elasticsearch") }}
#================================= Template =================================
setup.template.enabled: true
setup.template.name: {{ .Values.setupTemplate.name | quote }}
setup.template.pattern: {{ .Values.setupTemplate.pattern | quote }}
{{-       if .Values.setupTemplate.fields }}
setup.template.fields: {{ .Values.setupTemplate.fields | quote }}
{{-       end }}
setup.template.overwrite: {{ .Values.setupTemplate.overwrite }}
setup.template.settings:
  index:
    number_of_shards: {{ int .Values.setupTemplate.settings.index.numberOfShards }}
    codec: {{ .Values.setupTemplate.settings.index.codec }}
    number_of_routing_shards: {{ int .Values.setupTemplate.settings.index.numberOfRoutingShards }}
    mapping.total_fields.limit: {{ int .Values.setupTemplate.settings.index.mappingTotalFieldsLimit }}
{{-     end }}
#================================ Outputs =================================
{{-     if eq .Values.outputType "elasticsearch" }}
#-------------------------- Elasticsearch output --------------------------
output.elasticsearch:
  enabled: true
  {{-     with .Values.elasticsearch.hosts }}
  hosts:
    {{-     toYaml . | nindent 4 }}
  {{-     end }}
  compression_level: {{ int .Values.elasticsearch.compressionLevel }}
  protocol: "http"
  worker: {{ int .Values.elasticsearch.worker }}
  {{-     if .Values.elasticsearch.index }}
  index: {{ .Values.elasticsearch.index | quote }}
  {{-     end }}
  {{-     if .Values.elasticsearch.indices }}
  indices:
    {{-     range $i, $val := .Values.elasticsearch.indices }}
    - index: {{ $val.index | quote }}
      when.contains:
        processor.event: {{ $val.whenContains.processorEvent | quote }}
    {{-     end }}
  {{-     end }}
  {{-     if .Values.elasticsearch.pipeline }}
  pipeline: {{ .Values.elasticsearch.pipeline | quote }}
  {{-     end }}
  {{-     if .Values.elasticsearch.path }}
  path: {{ .Values.elasticsearch.path | quote }}
  {{-     end }}
  {{-     if .Values.elasticsearch.headers }}
  {{-       with .Values.elasticsearch.headers }}
  headers:
    {{- toYaml . | nindent 4 }}
  {{-       end }}
  {{-     end }}
  {{-     if .Values.elasticsearch.proxyUrl }}
  proxy_url: {{ .Values.elasticsearch.proxyUrl }}
  {{-     end }}
  max_retries: {{ int .Values.elasticsearch.maxRetries }}
  bulk_max_size: {{ int .Values.elasticsearch.bulkMaxSize }}
  backoff.init: {{ .Values.elasticsearch.backoffInit }}
  backoff.max: {{ .Values.elasticsearch.backoffMax }}
  timeout: {{ int .Values.elasticsearch.timeout }}
{{-     end }}
{{-     if eq .Values.outputType "console" }}
#----------------------------- Console output -----------------------------
output.console:
  enabled: true
  codec.json:
    pretty: {{ .Values.console.json.pretty }}
    escape_html: {{ .Values.console.json.escapeHtml }}
{{-     end }}
{{-     if eq .Values.outputType "logstash" }}
#---------------------------- Logstash output -----------------------------
output.logstash:
  enabled: true
  {{-     with .Values.logstash.hosts }}
  hosts:
    {{-     toYaml . | nindent 4 }}
  {{-     end }}
  compression_level: {{ .Values.logstash.compressionLevel }}
  worker: {{ .Values.logstash.worker }}
  escape_html: {{ .Values.logstash.escapeHtml }}
  ttl: {{ .Values.logstash.ttl }}
  loadbalance: {{ .Values.logstash.loadbalance }}
  pipelining: {{ .Values.logstash.pipelining }}
  slow_start: {{ .Values.logstash.slowStart }}
  backoff.init: {{ .Values.logstash.backoffInit }}
  backoff.max: {{ .Values.logstash.backoffMax }}
  {{-     if .Values.logstash.index }}
  index: {{ .Values.logstash.index }}
  {{-     end }}
  {{-     if .Values.logstash.proxyUrl }}
  proxy_url: {{ .Values.logstash.proxyUrl }}
  proxy_use_local_resolver: {{ .Values.logstash.proxyUseLocalResolver }}
  {{-     end }}
{{-     end }}
#============================= Instrumentation =============================
instrumentation:
  enabled: false
#================================= Paths ==================================
path.data: {{ .Values.dataDir.mountPath }}
path.logs: {{ .Values.logsDir.mountPath }}
#================================= Logging =================================
logging.level: {{ .Values.logLevel }}
logging.to_syslog: false
logging.to_files: false
logging.json: {{ .Values.logJson }}
logging.ecs: false
#=============================== HTTP Endpoint ===============================
{{-     if eq (int .Values.httpPort) 0 }}
http.enabled: false
{{-     else }}
http.enabled: true
http.host: 0.0.0.0
http.port: {{ int .Values.httpPort }}
{{-     end }}
{{-   end -}}
{{- end -}}
